# Docker install Centos

1. Install yum-utils

```
$ sudo yum install -y yum-utils
```

Verify that yum-utils installed or not.

```
$rpm -qa | grep yum-utils 
```

2. configure yum repo

```
$ sudo yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

3. Install docker ce and container.d

```
sudo yum install docker-ce docker-ce-cli containerd.io
```

Before running below numbered command- run this command to start Docker daemon

```
$sudo systemctl start docker
$sudo systemctl enable docker
```

By default only root users can access docker command, but we will not get root user access in real time.
the standard is when you install docker, a group caled docker is created automatically. your user should be part of that group.

```
$sudo usermod -aG docker centos
```

To get this effected you need to re log-in.