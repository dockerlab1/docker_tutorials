# lab-setup

1. Create google cloud account

2. Create a VM with Centos 7 OS

3. Create a firewall to open all the ports

4. generate public key and private key

```
ssh-keygen -f [file-name]
```

5. Don't give any pass phrase.

6. uplpad public key to google cloud metadata

7. using puttygen convert your pem file into private key

8. use below command to log-in to server

```
ssh -i [pem-file] [username]@[ip-address]