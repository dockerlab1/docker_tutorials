#!/bin/bash
# curl [scrip-url] | sudo bash
# curl https://gitlab.com/k805/lab-setup/-/raw/master/docker-install.sh | sudo bash

USERID=$(id -u)

if [ $USERID != 0 ]; then
    echo "You should be root user to run this script"
    exit 1
fi

yum install -y yum-utils -y

yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

yum install docker-ce docker-ce-cli containerd.io -y

id centos &> /dev/null

if [ $? == 0 ]; then
    usermod -aG docker centos
else
    echo "Your user name should be centos or change the username in the script"
    exit 1
fi


systemctl start docker

systemctl enable docker

echo -e "\e[31m \n\t======you need to re login=========\e[0m"